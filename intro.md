# introduction

every section contains some content from the **getting git** course  

## table of contents
section|1|2|3|4
--|--|--|--|--
|content|6to18|20to28|30to40|40to50

basic *git* configurations 
* author name
        git config --global user.name "username"
* author email 
        git config --global user.email "useremail"
* default editor
        git config --global core.editor "editor name or address"
* listing user configurations 
        git config --help 

    