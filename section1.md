# first section git,init,add,commit,remove,reset,revert

## git

*git* is a ditributed version control for managing files

## init

initilazing an empty repository (empty means it has not any changes)
turning a directory into a **git repository**   =>
    git init
getting more information by => 
    git status

## add 

stagging our changes
for tracking files we must add them by =>
    git add filename
some other useful commands =>  

    * git add .
    * git add -A
    * git add -u 
    * git add -p  

## commit

recording our stagges => git commit -m"message"(if we dont provide any message an editer will pop up)
every commit has an unique hashcode ill call them commit-unicode(u-c for short)

some other useful commands =>  

    * git commit -a -m "" 
    * git commit --amend
(if we dont care about rewriting message + but we wnnna change commit)
    * git commit -a --amend--no-edit 

## rm
 
removing from repo and filesystem =>
    git rm filename 
removeing from version controll =>
    git rm --cached filename

## reset

for unstagging changes =>git reset filename

    * git reset hashcode
    * git reset --hard hashcode

## revert
    git revert c-u
new commit to reverse the effect of a previous commit