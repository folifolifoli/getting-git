# section 2

## viewing history

## revisiting git status 
1. branch name
2. current state of repo
3. nxt possible command 

## log
older commits (commitunicode-author-date-message)
    git log -n (last n commits (n=0,1,2,...))
    git log --oneline (brief info)
    git log name (specific file logs)
    git log --graph b(sth like a tree in discerete math)
HEAD => last commit
HEAD-n,HEAD^*n (n=prev commits number)
last commit commit-unicode=HEAD

## show 

identical to log + changes in a commit
    git show c-u

## diff
viewing changes for modifies files 
    git diff
    git diff filename
    git diff c-u
    git diff c-u1..c-u2
