# section 3

*c-u means commit-hashtag*
## git branches & checkout
branch is used for managing out multiple branches at the same time
listing all branches
    git branch
create one
    git branch name
going into a branch
    git checkout name
going back to certain commit in time
    git checkout -b name c-u
before switching branches we must be in a clean state

## merge
combining branches
    git merge brancname
**conflicts** happen when git tries to merge (both) modified files in two seperated branches

## rebase
merge + reapplly any commits on top(getting new commits from master plus making new changes)
    git rebase master

## cherry pick 
if we dont wanna merge all the commits or we wanna a specific commit
    git cherry pick c-u
    git cherry pick c-u1..c-u2
